package com.twuc.webApp;

/**
 * @Author: huyf
 * @Date: 2019-08-05 13:50
 */
public class Bag {

    private int size;

    public Bag() {
    }

    public Bag(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }
}
