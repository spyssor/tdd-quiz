package com.twuc.webApp;

import java.util.*;

/**
 * @Author: huyf
 * @Date: 2019-08-05 13:50
 */
public class Storage {

    private Map<Ticket, Bag> bagMap;
    private int capacity = 10;

    private int[] slots;

    private List sizeMessage = Arrays.asList("small", "medium", "big");

    public Storage() {
        bagMap = new HashMap<>();
    }

    public Storage(int capacity) {
        bagMap = new HashMap<>(capacity);
        this.capacity = capacity;
        slots = new int[]{10,10,10};
    }

    public Storage(int small, int medium, int big) {
        int size = small + medium + big;
        bagMap = new HashMap<>(size);
        this.capacity = size;
        slots = new int[]{small, medium, big};
    }

    public Ticket save(Bag bag) {

        if (bagMap.size() == capacity) {
            throw new IllegalArgumentException("Insufficient capacity");
        }

        Ticket ticket = new Ticket();

        bagMap.put(ticket, bag);

        return ticket;
    }

    public Ticket save(Bag bag, int slotSize) {

        if (bagMap.size() == capacity || slots[slotSize] == 0) {
            throw new IllegalArgumentException("Insufficient capacity");
        }

        if (bag.getSize() <= slotSize) {
            Ticket ticket = new Ticket();

            bagMap.put(ticket, bag);

            return ticket;
        }

        throw new IllegalArgumentException("Cannot save your bag: " +
                sizeMessage.get(bag.getSize()) + sizeMessage.get(slotSize));
    }


    public Bag retrieve(Ticket ticket) {

        if (ticket != null && bagMap.containsKey(ticket)) {
            return bagMap.get(ticket);
        }

        throw new IllegalArgumentException("Invalid Ticket");
    }

}
