package com.twuc.webApp;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @Author: huyf
 * @Date: 2019-08-05 13:46
 */

class ApplicationTest {


    @Test
    void should_return_ticket_when_saving_the_bag() {

        //given
        Storage storage = new Storage();
        Bag bag = new Bag();

        //when
        Ticket ticket = storage.save(bag);

        //then
        assertNotNull(ticket);
    }

    @Test
    void should_get_ticket_when_saving_nothing() {

        //given
        Storage storage = new Storage();
        Bag nothing = new Bag();

        //when
        Ticket ticket = storage.save(nothing);

        //then
        assertNotNull(ticket);
    }

    @Test
    void should_get_saved_bag_when_retrieving_bag_using_valid_ticket() {

        //given
        Storage storage = new Storage();
        Bag expectBag = new Bag(
        );
        Ticket ticket = storage.save(expectBag);

        //when
        Bag actual = storage.retrieve(ticket);

        //then
        assertSame(expectBag, actual);

    }

    @Test
    void should_get_my_bag_when_using_valid_ticket() {

        //given
        Storage storage = new Storage();
        Bag Bag = new Bag();
        Ticket ticket = storage.save(Bag);

        //when
        Bag actual = storage.retrieve(ticket);

        //then
        assertNotNull(actual);
    }

    @Test
    void should_get_an_error_message_when_retrieving_bag_using_invalid_ticket() {

        //given
        Storage storage = new Storage();
        Bag Bag = new Bag();
        storage.save(Bag);
        Ticket invalidTicket = new Ticket();

        //when
        //then
        assertThrows(IllegalArgumentException.class, () ->
            storage.retrieve(invalidTicket), "Invalid Ticket");
    }


    @Test
    void should_get_nothing_when_retrieving_bag_using_valid_ticket() {

        //given
        Storage storage = new Storage();
        Ticket ticket = storage.save(null);

        //when
        Bag expectNothing = storage.retrieve(ticket);

        //then
        assertNull(expectNothing);
    }


    @Test
    void should_get_ticket_when_saving_bag_with_2_capacity_storage() {

        //given
        Storage storage = new Storage(2);
        Bag bag = new Bag();

        //when
        Ticket expectedTicket = storage.save(bag);

        //then
        assertNotNull(expectedTicket);

    }

    @Test
    void should_get_message_when_saving_bag_with_full_storage() {

        //given
        Storage storage = new Storage(2);
        Bag bag = new Bag();
        Bag anotherBag = new Bag();
        storage.save(bag);
        storage.save(anotherBag);
        Bag expectedBag = new Bag();

        //when
        assertThrows(IllegalArgumentException.class, () -> storage.save(expectedBag), "Insufficient capacity");
    }

    @Test
    void should_return_ticket_and_message_when_saving_one_bag_and_another_bag_with_1_capacity() {

        //given
        Storage storage = new Storage(1);
        Bag storageBag = new Bag();
        Bag insufficientBag = new Bag();

        //when
        Ticket ticket = storage.save(storageBag);
        String expectedMessage = assertThrows(IllegalArgumentException.class, () -> storage.save(insufficientBag)).getMessage();
        assertNotNull(ticket);
        assertEquals("Insufficient capacity", expectedMessage);
    }


    @Test
    void should_get_ticket_when_saving_small_bag_to_big_empty_slot() {
        //given
        Bag bag = new Bag(0);
        Storage storage = new Storage(0, 0, 1);
        int bigSlot = 2;

        //when
        Ticket expectedTicked = storage.save(bag, bigSlot);

        //then
        assertNotNull(expectedTicked);
    }


    @Test
    void should_get_error_message_when_saving_big_bag_to_small_empty_slot() {

        //given
        Bag bag = new Bag(2);
        Storage storage = new Storage(1, 0, 0);
        int smallSlot = 0;

        //then
        assertThrows(IllegalArgumentException.class,
                () -> storage.save(bag, smallSlot), "Cannot save your bag: big small");
    }

    @Test
    void should_get_full_message_when_saving_medium_bag_to_big_full_slot() {
        //given
        Bag bag = new Bag(1);
        Bag givenBag = new Bag(1);
        Storage storage = new Storage(0, 0, 1);
        int bigSlot = 2;

        storage.save(givenBag, bigSlot);

        //then
        assertThrows(IllegalArgumentException.class,
                () -> storage.save(bag, bigSlot), "Cannot save your bag: big small");
    }


    @Test
    void should_get_ticket_when_saving_medium_bag_to_medium_empty_slot() {
        //given
        Bag bag = new Bag(1);
        Storage storage = new Storage(0, 1, 0);
        int mediumSlot = 1;

        //when
        Ticket expectedTicked = storage.save(bag, mediumSlot);

        //then
        assertNotNull(expectedTicked);
    }






































}